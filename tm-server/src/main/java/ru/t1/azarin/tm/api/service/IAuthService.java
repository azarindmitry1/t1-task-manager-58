package ru.t1.azarin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.SessionDto;
import ru.t1.azarin.tm.dto.model.UserDto;

import java.sql.SQLException;

public interface IAuthService {

    @NotNull
    SessionDto validateToken(@Nullable String token);

    @NotNull
    UserDto registry(@NotNull String login, @NotNull String password, @NotNull String email) throws SQLException;

    @NotNull
    String login(@Nullable String login, @Nullable String password) throws SQLException;

}
