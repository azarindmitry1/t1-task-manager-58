package ru.t1.azarin.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.azarin.tm.api.repository.dto.ITaskDtoRepository;
import ru.t1.azarin.tm.api.service.dto.ITaskDtoService;
import ru.t1.azarin.tm.dto.model.TaskDto;
import ru.t1.azarin.tm.enumerated.Sort;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.exception.entity.EntityNotFoundException;
import ru.t1.azarin.tm.exception.entity.TaskNotFoundException;
import ru.t1.azarin.tm.exception.field.DescriptionEmptyException;
import ru.t1.azarin.tm.exception.field.IdEmptyException;
import ru.t1.azarin.tm.exception.field.NameEmptyException;
import ru.t1.azarin.tm.exception.field.UserIdEmptyException;

import javax.persistence.EntityManager;
import java.util.Collections;
import java.util.List;

@Service
@NoArgsConstructor
public final class TaskDtoService extends AbstractUserOwnedDtoService<TaskDto, ITaskDtoRepository> implements ITaskDtoService {

    @NotNull
    public ITaskDtoRepository getRepository() {
        return context.getBean(ITaskDtoRepository.class);
    }

    @Override
    public void add(@Nullable final TaskDto model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskRepository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public TaskDto create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final TaskDto task = new TaskDto();
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        task.setUserId(userId);
        task.setName(name);
        try {
            entityManager.getTransaction().begin();
            taskRepository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @NotNull
    @Override
    public TaskDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDto task = new TaskDto();
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        task.setUserId(userId);
        task.setName(name);
        task.setDescription(description);
        try {
            entityManager.getTransaction().begin();
            taskRepository.add(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return task;
    }

    @Override
    public void changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDto task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        task.setStatus(status);
        try {
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<TaskDto> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            return taskRepository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<TaskDto> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            return taskRepository.findAll(userId, sort);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<TaskDto> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            return taskRepository.findAllByProjectId(userId, projectId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public TaskDto findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            return taskRepository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final TaskDto model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskRepository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final TaskDto task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskRepository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@Nullable final TaskDto model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            taskRepository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final TaskDto task = findOneById(userId, id);
        if (task == null) throw new TaskNotFoundException();
        @NotNull final ITaskDtoRepository taskRepository = getRepository();
        @NotNull final EntityManager entityManager = taskRepository.getEntityManager();
        task.setName(name);
        task.setDescription(description);
        try {
            entityManager.getTransaction().begin();
            taskRepository.update(task);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}