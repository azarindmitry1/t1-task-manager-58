package ru.t1.azarin.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.azarin.tm.api.repository.dto.IUserDtoRepository;
import ru.t1.azarin.tm.dto.model.UserDto;

import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class UserDtoRepository extends AbstractDtoRepository<UserDto> implements IUserDtoRepository {

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM UserDto";
        entityManager.createQuery(jpql)
                .executeUpdate();
    }

    @Nullable
    @Override
    public List<UserDto> findAll() {
        @NotNull final String jpql = "SELECT m FROM UserDto";
        return entityManager.createQuery(jpql, UserDto.class)
                .setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

    @Nullable
    @Override
    public UserDto findOneById(@NotNull String id) {
        @NotNull final String jpql = "SELECT m FROM UserDto m WHERE m.id = :id";
        return entityManager.createQuery(jpql, UserDto.class)
                .setParameter("id", id)
                .setMaxResults(1)
                .setHint("org.hibernate.cacheable", true)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public UserDto findByLogin(@NotNull final String login) {
        @NotNull final String jpql = "SELECT m FROM UserDto m WHERE m.login = :login";
        return entityManager.createQuery(jpql, UserDto.class)
                .setParameter("login", login)
                .setMaxResults(1)
                .setHint("org.hibernate.cacheable", true)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Nullable
    @Override
    public UserDto findByEmail(@NotNull final String email) {
        @NotNull final String jpql = "SELECT m FROM UserDto m WHERE m.email = :email";
        return entityManager.createQuery(jpql, UserDto.class)
                .setParameter("email", email)
                .setMaxResults(1)
                .setHint("org.hibernate.cacheable", true)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull String id) {
        @NotNull final String jpql = "DELETE FROM UserDto m WHERE m.id = :id";
        entityManager.createQuery(jpql)
                .setParameter("id", id)
                .executeUpdate();
    }

}


