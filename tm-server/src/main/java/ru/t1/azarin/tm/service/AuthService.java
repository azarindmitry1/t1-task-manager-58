package ru.t1.azarin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.azarin.tm.api.service.IAuthService;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.api.service.dto.ISessionDtoService;
import ru.t1.azarin.tm.api.service.dto.IUserDtoService;
import ru.t1.azarin.tm.dto.model.SessionDto;
import ru.t1.azarin.tm.dto.model.UserDto;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.exception.field.LoginEmptyException;
import ru.t1.azarin.tm.exception.field.PasswordEmptyException;
import ru.t1.azarin.tm.exception.user.AccessDeniedException;
import ru.t1.azarin.tm.exception.user.IncorrectLoginOrPasswordException;
import ru.t1.azarin.tm.exception.user.UserIsLockedException;
import ru.t1.azarin.tm.util.CryptUtil;
import ru.t1.azarin.tm.util.HashUtil;

import java.util.Date;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class AuthService implements IAuthService {

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserDtoService userService;

    @NotNull
    @Autowired
    private ISessionDtoService sessionService;

    @NotNull
    @Override
    public UserDto registry(@NotNull final String login, @NotNull final String password, @NotNull final String email) {
        return userService.create(login, password, email);
    }

    @NotNull
    @Override
    public String login(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDto user = userService.findByLogin(login);
        if (user == null) throw new IncorrectLoginOrPasswordException();
        if (user.isLocked()) throw new UserIsLockedException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new IncorrectLoginOrPasswordException();
        if (!hash.equals(user.getPasswordHash())) throw new IncorrectLoginOrPasswordException();
        return getToken(user);
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final UserDto user) {
        return getToken(createSession(user));
    }

    @NotNull
    @SneakyThrows
    private String getToken(@NotNull final SessionDto session) {
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String token = objectMapper.writeValueAsString(session);
        @NotNull final String sessionKey = propertyService.getSessionKey();
        return CryptUtil.encrypt(sessionKey, token);
    }

    @NotNull
    private SessionDto createSession(@NotNull final UserDto user) {
        @NotNull final SessionDto session = new SessionDto();
        session.setUserId(user.getId());
        @NotNull final Role role = user.getRole();
        session.setRole(role);
        sessionService.add(session);
        return session;
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDto validateToken(@Nullable final String token) {
        if (token == null || token.isEmpty()) throw new AccessDeniedException();
        @NotNull final String sessionKey = propertyService.getSessionKey();
        @NotNull final String json;
        try {
            json = CryptUtil.decrypt(sessionKey, token);
        } catch (@NotNull final Exception e) {
            throw new AccessDeniedException();
        }
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final SessionDto session = objectMapper.readValue(json, SessionDto.class);
        @NotNull final Date currentDate = new Date();
        @NotNull final Date sessionDate = session.getDate();
        final long delta = (currentDate.getTime() - sessionDate.getTime()) / 1000;
        @NotNull final int timeout = propertyService.getSessionTimeout();
        if (delta > timeout) throw new AccessDeniedException();
        if (!sessionService.existsById(session.getUserId(), session.getId())) throw new AccessDeniedException();
        return session;
    }

}
