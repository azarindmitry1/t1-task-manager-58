package ru.t1.azarin.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.UserDto;

import java.util.List;

public interface IUserDtoRepository extends IDtoRepository<UserDto> {

    void clear();

    @Nullable
    List<UserDto> findAll();

    @Nullable
    UserDto findOneById(@NotNull String id);

    @Nullable
    UserDto findByLogin(@NotNull String login);

    @Nullable
    UserDto findByEmail(@NotNull String email);

    void removeById(@NotNull String id);

}
