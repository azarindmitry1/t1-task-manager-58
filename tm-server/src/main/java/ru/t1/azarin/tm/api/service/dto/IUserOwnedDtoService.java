package ru.t1.azarin.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.azarin.tm.dto.model.AbstractUserOwnedDtoModel;
import ru.t1.azarin.tm.enumerated.Sort;

import java.util.List;

public interface IUserOwnedDtoService<M extends AbstractUserOwnedDtoModel> extends IDtoService<M> {

    void clear(@Nullable String userId);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<M> findAll(@Nullable String userId);

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    @Nullable
    M findOneById(@Nullable String userId, @Nullable String id);

    void removeById(@Nullable String userId, @Nullable String id);

}
