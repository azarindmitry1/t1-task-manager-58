package ru.t1.azarin.tm.repository.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.azarin.tm.api.repository.model.IRepository;
import ru.t1.azarin.tm.model.AbstractModel;

import javax.persistence.EntityManager;

@Getter
@Repository
@NoArgsConstructor
@Scope("prototype")
public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @NotNull
    @Autowired
    protected EntityManager entityManager;

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

}
