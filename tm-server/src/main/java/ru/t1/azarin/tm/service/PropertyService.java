package ru.t1.azarin.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.azarin.tm.api.service.IPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @Value("#{environment['password.secret']}")
    private String passwordSecret;

    @Value("#{environment['password.iteration']}")
    private Integer passwordIteration;

    @Value("#{environment['database.username']}")
    private String databaseUser;

    @Value("#{environment['database.password']}")
    private String databasePassword;

    @Value("#{environment['database.url']}")
    private String databaseUrl;

    @Value("#{environment['database.driver']}")
    private String databaseDriver;

    @Value("#{environment['database.dialect']}")
    private String databaseDialect;

    @Value("#{environment['database.hbm2ddl_auto']}")
    private String databaseHbm2ddlAuto;

    @Value("#{environment['database.show_sql']}")
    private String databaseShowSql;

    @Value("#{environment['database.format_sql']}")
    private String databaseFormatSql;

    @Value("#{environment['database.second_lvl_cache']}")
    private String databaseSecondLvlCache;

    @Value("#{environment['database.factory_class']}")
    private String databaseFactoryClass;

    @Value("#{environment['database.use_query_cache']}")
    private String databaseUseQueryCache;

    @Value("#{environment['database.use_min_puts']}")
    private String databaseUseMinPuts;

    @Value("#{environment['database.region_prefix']}")
    private String databaseRegionPrefix;

    @Value("#{environment['database.config_file_path]}")
    private String databaseConfigFilePath;

    @Value("#{environment['server.port]}")
    private String serverPort;

    @Value("#{environment['server.host]}")
    private String serverHost;

    @Value("#{environment['session.key]}")
    private String sessionKey;

    @Value("#{environment['session.timeout]}")
    private Integer sessionTimeout;

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

}