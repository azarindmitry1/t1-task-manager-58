package ru.t1.azarin.tm.service.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import ru.t1.azarin.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.azarin.tm.api.service.dto.IProjectDtoService;
import ru.t1.azarin.tm.dto.model.ProjectDto;
import ru.t1.azarin.tm.enumerated.Sort;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.exception.entity.EntityNotFoundException;
import ru.t1.azarin.tm.exception.entity.ProjectNotFoundException;
import ru.t1.azarin.tm.exception.field.DescriptionEmptyException;
import ru.t1.azarin.tm.exception.field.IdEmptyException;
import ru.t1.azarin.tm.exception.field.NameEmptyException;
import ru.t1.azarin.tm.exception.field.UserIdEmptyException;

import javax.persistence.EntityManager;
import java.util.List;

@Service
@NoArgsConstructor
public final class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDto, IProjectDtoRepository> implements IProjectDtoService {

    @NotNull
    public IProjectDtoRepository getRepository() {
        return context.getBean(IProjectDtoRepository.class);
    }

    @Override
    public void add(@Nullable final ProjectDto model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepository.add(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @NotNull
    @Override
    public ProjectDto create(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @Nullable final ProjectDto project = new ProjectDto();
        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        project.setUserId(userId);
        project.setName(name);
        try {
            entityManager.getTransaction().begin();
            projectRepository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @NotNull
    @Override
    public ProjectDto create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDto project = new ProjectDto();
        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        try {
            entityManager.getTransaction().begin();
            projectRepository.add(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
        return project;
    }

    @Override
    public void changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDto project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        project.setStatus(status);
        try {
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepository.clear(userId);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<ProjectDto> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            return projectRepository.findAll(userId);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public List<ProjectDto> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (sort == null) return findAll(userId);
        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            return projectRepository.findAll(userId, sort);
        } finally {
            entityManager.close();
        }
    }

    @Nullable
    @Override
    public ProjectDto findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            return projectRepository.findOneById(userId, id);
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void remove(@Nullable final ProjectDto model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepository.remove(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDto project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepository.removeById(userId, id);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void update(@Nullable final ProjectDto model) {
        if (model == null) throw new EntityNotFoundException();
        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        try {
            entityManager.getTransaction().begin();
            projectRepository.update(model);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

    @Override
    public void updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDto project = findOneById(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        @NotNull final IProjectDtoRepository projectRepository = getRepository();
        @NotNull final EntityManager entityManager = projectRepository.getEntityManager();
        project.setName(name);
        project.setDescription(description);
        try {
            entityManager.getTransaction().begin();
            projectRepository.update(project);
            entityManager.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            entityManager.getTransaction().rollback();
            throw e;
        } finally {
            entityManager.close();
        }
    }

}