package ru.t1.azarin.tm.repository.dto;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.azarin.tm.api.repository.dto.IProjectDtoRepository;
import ru.t1.azarin.tm.dto.model.ProjectDto;
import ru.t1.azarin.tm.enumerated.Sort;

import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class ProjectDtoRepository extends AbstractUserOwnedDtoRepository<ProjectDto> implements IProjectDtoRepository {

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final String jpql = "DELETE FROM ProjectDto m WHERE m.userId = :userId";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Nullable
    @Override
    public List<ProjectDto> findAll(@NotNull final String userId) {
        @NotNull final String jpql = "SELECT m FROM ProjectDto m WHERE m.userId = :userId";
        return entityManager.createQuery(jpql, ProjectDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public List<ProjectDto> findAll(@NotNull final String userId, @NotNull final Sort sort) {
        @NotNull final String jpql = String.format("SELECT m FROM ProjectDto m WHERE m.userId = :userId ORDER_BY m.%s",
                getSortColumn(sort.getComparator()));
        return entityManager.createQuery(jpql, ProjectDto.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDto findOneById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "SELECT m FROM ProjectDto m WHERE m.userId = :userId AND m.id = :id";
        return entityManager.createQuery(jpql, ProjectDto.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1)
                .getResultList().stream().findFirst().orElse(null);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final String jpql = "DELETE FROM ProjectDto m WHERE m.userId = :userId AND m.id = :id";
        entityManager.createQuery(jpql)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .executeUpdate();
    }

}
