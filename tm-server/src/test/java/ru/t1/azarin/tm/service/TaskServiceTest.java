package ru.t1.azarin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.azarin.tm.api.service.IPropertyService;
import ru.t1.azarin.tm.api.service.dto.ITaskDtoService;
import ru.t1.azarin.tm.api.service.dto.IUserDtoService;
import ru.t1.azarin.tm.configuration.ServerConfiguration;
import ru.t1.azarin.tm.dto.model.TaskDto;
import ru.t1.azarin.tm.dto.model.UserDto;
import ru.t1.azarin.tm.enumerated.Status;
import ru.t1.azarin.tm.exception.entity.EntityNotFoundException;
import ru.t1.azarin.tm.exception.field.DescriptionEmptyException;
import ru.t1.azarin.tm.exception.field.IdEmptyException;
import ru.t1.azarin.tm.exception.field.NameEmptyException;
import ru.t1.azarin.tm.exception.field.UserIdEmptyException;
import ru.t1.azarin.tm.marker.UnitCategory;
import ru.t1.azarin.tm.util.HashUtil;

import java.util.Collections;

import static ru.t1.azarin.tm.constant.ProjectTestData.USER1_PROJECT1;
import static ru.t1.azarin.tm.constant.TaskTestData.USER1_TASK1;
import static ru.t1.azarin.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private static IPropertyService PROPERTY_SERVICE;

    @NotNull
    private static ITaskDtoService TASK_SERVICE;

    @NotNull
    private static IUserDtoService USER_SERVICE;

    @NotNull
    private static String USER_ID = "";

    @NotNull
    private final String emptyString = "";

    @Nullable
    private final String nullString = null;

    @NotNull
    private final String testString = "testString";

    @BeforeClass
    public static void setUp() {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        PROPERTY_SERVICE = context.getBean(IPropertyService.class);
        TASK_SERVICE = context.getBean(ITaskDtoService.class);
        USER_SERVICE = context.getBean(IUserDtoService.class);
        @NotNull final UserDto user = new UserDto();
        user.setLogin(USER1_LOGIN);
        user.setPasswordHash(HashUtil.salt(PROPERTY_SERVICE, USER1_PASSWORD));
        user.setEmail(USER1_EMAIL);
        USER_SERVICE.add(user);
        USER_ID = user.getId();
    }

    @AfterClass
    public static void tearDown() {
        @Nullable final UserDto user = USER_SERVICE.findByLogin(USER1_LOGIN);
        USER_SERVICE.remove(user);
    }

    @Before
    public void before() {
        USER1_TASK1.setUserId(USER_ID);
        TASK_SERVICE.add(USER1_TASK1);
    }

    @After
    public void after() {
        TASK_SERVICE.clear(USER_ID);
    }

    @Test
    public void add() {
        Assert.assertThrows(EntityNotFoundException.class, () -> TASK_SERVICE.add(null));
        TASK_SERVICE.clear(USER_ID);
        Assert.assertEquals(0, TASK_SERVICE.findAll(USER_ID).size());
        @NotNull final TaskDto task = new TaskDto();
        task.setName(testString);
        task.setDescription(testString);
        task.setUserId(USER_ID);
        TASK_SERVICE.add(task);
        Assert.assertEquals(1, TASK_SERVICE.findAll(USER_ID).size());
    }

    @Test
    public void create() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create(
                emptyString, USER1_TASK1.getName(), USER1_TASK1.getDescription())
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.create(
                nullString, USER1_TASK1.getName(), USER1_TASK1.getDescription())
        );
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(
                USER_ID, emptyString, USER1_TASK1.getDescription())
        );
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.create(
                USER_ID, nullString, USER1_TASK1.getDescription())
        );
        Assert.assertThrows(DescriptionEmptyException.class, () -> TASK_SERVICE.create(
                USER_ID, USER1_TASK1.getName(), emptyString)
        );
        Assert.assertThrows(DescriptionEmptyException.class, () -> TASK_SERVICE.create(
                USER_ID, USER1_TASK1.getName(), nullString)
        );
        @Nullable final TaskDto task = TASK_SERVICE.create(USER_ID, testString, testString);
        Assert.assertEquals(testString, task.getName());
        Assert.assertEquals(testString, task.getDescription());
    }

    @Test
    public void clear() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.clear(emptyString));
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.clear(nullString));
        TASK_SERVICE.clear(USER_ID);
        Assert.assertEquals(0, TASK_SERVICE.findAll(USER_ID).size());
    }

    @Test
    public void changeProjectStatusById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(
                emptyString, USER1_TASK1.getId(), Status.COMPLETED)
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(
                nullString, USER1_TASK1.getId(), Status.COMPLETED)
        );
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(
                USER_ID, emptyString, Status.COMPLETED)
        );
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.changeTaskStatusById(
                USER_ID, nullString, Status.COMPLETED)
        );
        TASK_SERVICE.changeTaskStatusById(USER_ID, USER1_TASK1.getId(), Status.COMPLETED);
        @Nullable final TaskDto task = TASK_SERVICE.findOneById(USER_ID, USER1_TASK1.getId());
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test
    public void existsById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.existsById(emptyString, USER1_TASK1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.existsById(nullString, USER1_TASK1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.existsById(USER_ID, emptyString));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.existsById(USER_ID, nullString));
        @Nullable final TaskDto task = TASK_SERVICE.create(USER_ID, testString, testString);
        Assert.assertTrue(TASK_SERVICE.existsById(USER_ID, task.getId()));
        Assert.assertFalse(TASK_SERVICE.existsById(USER_UNREGISTRY_ID, USER1_TASK1.getId()));
    }

    @Test
    public void findAll() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.findAll(emptyString));
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.findAll(nullString));
        Assert.assertEquals(1, TASK_SERVICE.findAll(USER_ID).size());
        Assert.assertEquals(0, TASK_SERVICE.findAll(USER_UNREGISTRY_ID).size());
    }

    @Test
    public void findOneById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.findOneById(emptyString, USER1_TASK1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.findOneById(nullString, USER1_TASK1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.findOneById(USER_ID, emptyString));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.findOneById(USER_ID, nullString));
        @Nullable final TaskDto task = TASK_SERVICE.findOneById(USER_ID, USER1_TASK1.getId());
        Assert.assertEquals(USER1_TASK1.getId(), task.getId());
    }

    @Test
    public void findAllByProjectId() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.findAllByProjectId(
                emptyString, USER1_PROJECT1.getId())
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.findAllByProjectId(
                nullString, USER1_PROJECT1.getId())
        );
        Assert.assertEquals(TASK_SERVICE.findAllByProjectId(USER_ID, nullString), Collections.emptyList());
    }

    @Test
    public void remove() {
        Assert.assertThrows(EntityNotFoundException.class, () -> TASK_SERVICE.remove(null));
        @Nullable final TaskDto task = TASK_SERVICE.findOneById(USER_ID, USER1_TASK1.getId());
        TASK_SERVICE.remove(task);
        Assert.assertEquals(0, TASK_SERVICE.findAll(USER_ID).size());
    }

    @Test
    public void removeById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.removeById(emptyString, USER1_TASK1.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.removeById(nullString, USER1_TASK1.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.findOneById(USER_ID, emptyString));
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.findOneById(USER_ID, nullString));
        TASK_SERVICE.removeById(USER_ID, USER1_TASK1.getId());
        Assert.assertEquals(0, TASK_SERVICE.findAll(USER_ID).size());
    }

    @Test
    public void updateById() {
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.updateById(
                emptyString, USER1_TASK1.getId(), USER1_TASK1.getName(), USER1_TASK1.getDescription())
        );
        Assert.assertThrows(UserIdEmptyException.class, () -> TASK_SERVICE.updateById(
                nullString, USER1_TASK1.getId(), USER1_TASK1.getName(), USER1_TASK1.getDescription())
        );
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.updateById(
                USER_ID, emptyString, USER1_TASK1.getName(), USER1_TASK1.getDescription())
        );
        Assert.assertThrows(IdEmptyException.class, () -> TASK_SERVICE.updateById(
                USER_ID, nullString, USER1_TASK1.getName(), USER1_TASK1.getDescription())
        );
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.updateById(
                USER_ID, USER1_TASK1.getId(), emptyString, USER1_TASK1.getDescription())
        );
        Assert.assertThrows(NameEmptyException.class, () -> TASK_SERVICE.updateById(
                USER_ID, USER1_TASK1.getId(), nullString, USER1_TASK1.getDescription())
        );
        Assert.assertThrows(DescriptionEmptyException.class, () -> TASK_SERVICE.updateById(
                USER_ID, USER1_TASK1.getId(), USER1_TASK1.getName(), emptyString)
        );
        Assert.assertThrows(DescriptionEmptyException.class, () -> TASK_SERVICE.updateById(
                USER_ID, USER1_TASK1.getId(), USER1_TASK1.getName(), nullString)
        );
        TASK_SERVICE.updateById(USER_ID, USER1_TASK1.getId(), testString, USER1_TASK1.getDescription());
        @Nullable final TaskDto task = TASK_SERVICE.findOneById(USER_ID, USER1_TASK1.getId());
        Assert.assertEquals(testString, task.getName());
    }

}
