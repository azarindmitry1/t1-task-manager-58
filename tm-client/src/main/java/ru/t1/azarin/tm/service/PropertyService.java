package ru.t1.azarin.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.t1.azarin.tm.api.service.IPropertyService;

@Getter
@Service
public final class PropertyService implements IPropertyService {

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @Value("#{environment['server.port]}")
    private String serverPort;

    @Value("#{environment['server.host]}")
    private String serverHost;

    @NotNull
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

}
