package ru.t1.azarin.tm.listener.system;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.azarin.tm.api.endpoint.ISystemEndpoint;
import ru.t1.azarin.tm.enumerated.Role;
import ru.t1.azarin.tm.listener.AbstractListener;

import java.util.List;

@Getter
@Component
public abstract class AbstractSystemListener extends AbstractListener {

    @NotNull
    @Autowired
    protected ISystemEndpoint systemEndpoint;

    @NotNull
    @Autowired
    private List<AbstractListener> listeners;

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
